import { Directive, ElementRef, HostListener } from '@angular/core';
import { SVGService } from '../services/svgservice.service';

@Directive({
  selector: '[appDraggable]'
})
export class DraggableDirective {
  constructor(private el: ElementRef, private svgService: SVGService) {
    this.el.nativeElement.setAttribute('draggable', true);
  }

  @HostListener('dragstart', ['$event'])
  onDragStart(event) {
    let elementToBeDragged = event.target.querySelector('svg').children[0];
    this.svgService.setDraggedSvgShape(elementToBeDragged);
    event.dataTransfer.setData('text', elementToBeDragged.id);
  }

  @HostListener('document:dragover', ['$event'])
  onDragOver(event) {
      event.preventDefault();
  }
}