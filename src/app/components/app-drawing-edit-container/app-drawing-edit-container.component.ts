import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-drawing-edit-container',
  templateUrl: './app-drawing-edit-container.component.html',
  styleUrls: ['./app-drawing-edit-container.component.sass']
})
export class AppDrawingEditContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
