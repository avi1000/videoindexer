import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppDrawingEditContainerComponent } from './app-drawing-edit-container.component';

describe('AppDrawingEditContainerComponent', () => {
  let component: AppDrawingEditContainerComponent;
  let fixture: ComponentFixture<AppDrawingEditContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppDrawingEditContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppDrawingEditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
