import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-container',
  templateUrl: './app-header-container.component.html',
  styleUrls: ['./app-header-container.component.sass']
})
export class AppHeaderContainerComponent implements OnInit {

  public title = "Diagram drawer";
  constructor() { }

  ngOnInit() {
  }

}
