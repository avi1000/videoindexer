import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-drawing-view-container',
  templateUrl: './app-drawing-view-container.component.html',
  styleUrls: ['./app-drawing-view-container.component.sass']
})
export class AppDrawingViewContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
