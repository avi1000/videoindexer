import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppDrawingViewContainerComponent } from './app-drawing-view-container.component';

describe('AppDrawingViewContainerComponent', () => {
  let component: AppDrawingViewContainerComponent;
  let fixture: ComponentFixture<AppDrawingViewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppDrawingViewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppDrawingViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
