import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppShapesAreaComponent } from './app-shapes-area.component';

describe('AppShapesAreaComponent', () => {
  let component: AppShapesAreaComponent;
  let fixture: ComponentFixture<AppShapesAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppShapesAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppShapesAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
