import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppDrawingAreaComponent } from './app-drawing-area.component';

describe('AppDrawingAreaComponent', () => {
  let component: AppDrawingAreaComponent;
  let fixture: ComponentFixture<AppDrawingAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppDrawingAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppDrawingAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
