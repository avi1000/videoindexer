import { Component, OnInit } from '@angular/core';
import { DiagramsApiService } from 'src/app/services/DiagramsApi.service';

@Component({
  selector: 'app-drawing-area',
  templateUrl: './app-drawing-area.component.html',
  styleUrls: ['./app-drawing-area.component.sass']
})
export class AppDrawingAreaComponent implements OnInit {

  
  constructor(private apiService: DiagramsApiService) { }

  ngOnInit() {
  }

  public onShapeClicked(event) {
    if (event.target.tagName === "svg")
      return;   
  }

  

}
