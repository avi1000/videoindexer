import { Shape, IPosition } from './shape';

export class Rectangle extends Shape {
    constructor(domElement){
        super(domElement);
    }
    public getPosition(): IPosition{
        return {x: this.svgElement['x'].animVal.value, y: this.svgElement['y'].animVal.value};
    }

    public setPosition(position: IPosition){
        const xPosition = position.x - (this.svgElement['width'].animVal.value / 2);
        const yPosition = position.y - (this.svgElement['height'].animVal.value / 2);
        this.svgElement.setAttribute('x', xPosition.toString());
        this.svgElement.setAttribute('y', yPosition.toString());
    }
}