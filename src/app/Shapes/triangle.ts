import { Shape, IPosition } from './shape';

export class Triangle extends Shape {
    constructor(domElement){
        super(domElement);
    }
    public getPosition(): IPosition{
        return {x: 1, y: 3};
    }

    public setPosition(position: IPosition){
        
    }
}