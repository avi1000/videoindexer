import { ElementRef } from '@angular/core';

export interface IPosition {
    x: number;
    y: number;
}
export interface IShape {
   getPosition(): IPosition;
   setPosition(position: IPosition);
   getDomElement(): any;
}

export abstract class Shape implements IShape {
    
    protected svgElement: any;
    public abstract getPosition(): IPosition;
    public abstract setPosition(position: IPosition);
    
    protected constructor(element: ElementRef){
        this.svgElement = element;
    }
    getDomElement() {
        return this.svgElement;
    }
}