import { Shape, IPosition } from './shape';

export class Circle extends Shape {
    constructor(domElement){
        super(domElement);
    }
    public getPosition(): IPosition{
        return {x: this.svgElement['cx'], y: this.svgElement['cy']};
    }

    public setPosition(position: IPosition){
        this.svgElement.setAttribute('cx', position.x);
        this.svgElement.setAttribute('cy', position.y);
    }
}