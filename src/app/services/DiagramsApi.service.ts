import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API_URL = 'https://avitest1.azurewebsites.net/api/drawings';

@Injectable({
  providedIn: 'root'
})
export class DiagramsApiService {

  constructor(private httpClient: HttpClient) {

  }

  public saveDrawing(drawingData: any){
    this.httpClient.post(`${API_URL}`, JSON.stringify({
      "data": drawingData
  })).subscribe((a)=>{
      // returns diagramId created on the backend to use for the publish action
      debugger;
  });
  }

  public publishDrawing(diagramId: string){
    // sets a flag on the back for the diagramId to make it available
    this.httpClient.put(`${API_URL}`, JSON.stringify({
      "pId": diagramId
  })).subscribe((a)=>{
      debugger;
  });
  }

  public getDrawing(drawingId: string){
    this.httpClient.get(`${API_URL}?${drawingId}`,).subscribe((a)=>{
      debugger;
  });
  }
}
