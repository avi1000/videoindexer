import { Injectable } from '@angular/core';
import { IShape } from '../Shapes/shape';
import { ShapesFactory } from '../ShapesFactory';

@Injectable()
export class SVGService {
  private _svgShape: IShape;
  
  constructor() {}

  public getSVGPoint(event, element): SVGPoint {
    const point = this._svgShape.getDomElement().viewportElement.createSVGPoint();
    point.x = event.clientX;
    point.y = event.clientY;

    const CTM = element.viewportElement.getScreenCTM();
    const svgPoint = point.matrixTransform(CTM.inverse());

    return svgPoint;
  }
  
  public setDraggedSvgShape(svgElement){
    this._svgShape = ShapesFactory.getShape(svgElement);
  }

  public setShapePosition(coord) {
    this._svgShape.setPosition(coord);
  }

public attachClickEvent(element){
  // element.addEventListener('click', ()=>{
  //   this.setCoordinates(this._svgShape.getPosition());
  // });
}
  public drawConnectedLine(){

  }
}