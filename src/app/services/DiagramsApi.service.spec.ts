import { TestBed } from '@angular/core/testing';

import { DiagramsApiService } from './DiagramsApi.service';

describe('ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DiagramsApiService = TestBed.get(DiagramsApiService);
    expect(service).toBeTruthy();
  });
});
