import { Circle } from './Shapes/circle';
import { Rectangle } from './Shapes/rectangle';
import { Triangle } from './Shapes/triangle';

const lookupDictionary = {"circle": Circle, "rect": Rectangle, "path": Triangle};
export class ShapesFactory {
    public static getShape(element: HTMLElement){
        return new lookupDictionary[element.tagName](element);
    }
}