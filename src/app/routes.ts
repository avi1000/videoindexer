import { AppDrawingViewContainerComponent } from './components/app-drawing-view-container/app-drawing-view-container.component';
import { AppDrawingEditContainerComponent } from './components/app-drawing-edit-container/app-drawing-edit-container.component';
import { Routes } from '@angular/router';

export const APP_ROUTES: Routes = [
    { path: '', component: AppDrawingEditContainerComponent },
    { path: 'view', component: AppDrawingViewContainerComponent }
  ];