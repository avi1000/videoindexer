import { Component } from '@angular/core';
import { DiagramsApiService } from './services/DiagramsApi.service';

interface ICoordinates {
  x: number;
  y: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'drawing-app';

  constructor(private apiService: DiagramsApiService){

  }
  public onPublishClick(event){
    const svg = document.getElementById('dropArea')
    this.apiService.saveDrawing(svg.outerHTML);
  }   
}