import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppToolbarComponent } from './components/app-toolbar/app-toolbar.component';
import { AppShapesAreaComponent } from './components/app-shapes-area/app-shapes-area.component';
import { AppHeaderContainerComponent } from './components/app-header-container/app-header-container.component';
import { AppDrawingAreaComponent } from './components/app-drawing-area/app-drawing-area.component';
import { AppDrawingViewContainerComponent } from './components/app-drawing-view-container/app-drawing-view-container.component';
import { AppDrawingEditContainerComponent } from './components/app-drawing-edit-container/app-drawing-edit-container.component';
import { APP_ROUTES } from 'src/app/routes';
import { DraggableDirective } from './directives/draggable-directive.directive';
import { DroppableDirective } from './directives/droppable-directive.directive';
import { SVGService } from './services/svgservice.service';

@NgModule({
  declarations: [
    AppComponent,
    AppShapesAreaComponent,
    AppHeaderContainerComponent,
    AppToolbarComponent,
    AppShapesAreaComponent,
    AppDrawingAreaComponent,
    AppDrawingViewContainerComponent,
    AppDrawingEditContainerComponent,
    DraggableDirective,
    DroppableDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [SVGService],
  bootstrap: [AppComponent]
})
export class AppModule { }
